# Bobby

Standardised linting and testing tools for all python repositories.


## Robert Peel (1788 - 1850)
This framework is named in honour of [Robert Peel](https://en.wikipedia.org/wiki/Robert_Peel) (1788 - 1850), a British politician and the founder of the formalised and nationwide police service we have today. "Bobby" is still used today in the UK as a nickname for the police. Prior to Peel's reforms, individual mayors of towns might choose to appoint police, or not, and standards varied significantly with no centralised structure - this framework aims to achieve something similar for the linting and testing standards that protect our codebases.


## How to use
Bobby is not an installable library - rather it is a template. The concept is to enforce a unified minimum linting and testing standard across repositories. To use it you should review the files present here - `pyproject.toml`, `Makefile`, and `poetryversion.common`.

The first step is from `pyproject.toml` you should copy all the libraries into your own repository's `pyproject.toml` (or any other package management system, but this template assumes poetry as part of its standardisation). This ensures your linting will have all the expected components. Furthermore the exact standards for the installed tools (mypy, ruff, black, pytest) are defined in this file also.

Next you should copy `Makefile` into your project's root folder. Note, the various limiting and formatting commands run more efficiently when the package names are specified explicitly. As such, you will need to set the `PACKAGE_NAME` variable in the `Makefile` with the name of your package. The goal of using `Make` is that developers may move to an unfamiliar codebase but still issue standardised commands such as `make format` and so not be required to learn a new interface for each repository. However much like `poetry` as the package manager could be replaced, you could also use another format instead of `Make` and still be considered as following the required standard (it is rather the python format which is the standard itself); if you wish to do this you should just make sure you review the `Makefile` as it includes all the expected interactions.

Finally the purpose of `poetryversion.common` is simply a reference so that when poetry is used as the package manager then a standardised version is used both on the local machine and the remote machine used for the CICD pipeline.

Bear in mind that Bobby's library standards includes `pytest` and `coverage` and, again for more rapid understanding across codebases, you should use these testing libraries in preference to any alternatives (unittest, nose, etc). Included in `Makefile` is a standard recipe for:

```
make test
```
for running tests and
```
make coverage
```
for returning a coverage report.

Should you require a different format for tests in particular (this is most commonly the case where you require the use of docker-compose to run integration testing) you may replace the `make test` recipe within `Makefile`. This similarly applies for any other of the standard commands you require to modify to ensure the standard behaves as expected. Please review the `Makefile` for all the standardised commands.
