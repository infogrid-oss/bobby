# Please refer to: https://gitlab.com/infogrid-oss/bobby
# for Infogrid linting and testing standards

.DEFAULT_GOAL := help
SHELL := /bin/bash
SRC_PATH := bobby

.PHONY: black-fix
black-fix: ## Run black formatter
	@poetry run black $(SRC_PATH) tests;

.PHONY: black-check
black-check: ## Run black formatter
	@poetry run black $(SRC_PATH) tests --check;

.PHONY: clean
clean: ## Remove python cache files
	-@find . -name '*.pyc' -exec rm -f {} +;
	-@find . -name '*.pyo' -exec rm -f {} +;
	-@find . -name '*__pycache__' -exec rm -fr {} +;
	-@find . -name '*.mypy_cache' -exec rm -fr {} +;
	-@find . -name '*.pytest_cache' -exec rm -fr {} +;
	-@find . -name '.coverage' -exec rm -fr {} +;

.PHONY: coverage
coverage: ## Report test coverage
	@poetry run coverage report;

.PHONY: force-update
force-update: ## Forcefully update poetry.lock using pyproject.toml
	-@rm poetry.lock;
	@poetry update;

.PHONY: format
format: black-fix ruff-fix ## Format to match all linting requirements

.PHONY: help
help: ## Show all available commands
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-13s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST);

.PHONY: install
install: ## Install all packages using poetry.lock
	-@poetry config installer.max-workers 10;
	@poetry install --no-interaction --no-ansi -vvv;

.PHONY: mypy-check
mypy-check: ## Run mypy type checking
	@poetry run mypy $(SRC_PATH) tests;

.PHONY: quality
quality: black-check ruff-check mypy-check ## Run linting checks

.PHONY: ruff-check
ruff-check: ## Run ruff linting
	@poetry run ruff check $(SRC_PATH) tests;

.PHONY: ruff-fix
ruff-fix: ## Run ruff auto-fixing
	@poetry run ruff check --fix-only $(SRC_PATH) tests;

.PHONY: test
test: ## Run test pipeline
	@poetry run coverage run --source=. --append -m pytest;

.PHONY: uninstall
uninstall: ## Remove virtual environment
	@-rm -rf `poetry env info -p`;

.PHONY: update
update: ## Update poetry.lock using pyproject.toml
	@poetry update;
